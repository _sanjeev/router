import React, { Component } from 'react';

class Home extends Component {
    constructor(props) {
        super(props);
        console.log(this.props);
    }
    render() {
        return (
            <div>
                {this.props.name}
            </div>
        );
    }
}

export default Home;