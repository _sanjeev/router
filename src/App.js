import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route, Routes, Switch } from 'react-router-dom';
import About from './About';
import Home from './Home';
import Login from './Login';
import Nav from './Nav';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'sahil',
    }
  }

  changeState = () => {
    let obj = {
      name : "sahil",
    }
    obj.name = 'raju';
    this.setState(obj);
  }

  render() {
    return (
      <Router>
        <Nav />

        <button onClick={this.changeState}>change State</button>

        <Switch>

          <Route exact path="/" render={(props) => {
            return <Home name={this.state.name} {...props}/>
          }} />
          <Route exact path="/about" component={About} />
          <Route exact path="/login" component={Login} />

        </Switch>
      </Router>
    );
  }
}

export default App;